<!-- 1. Инкапсуляция - выделение общего, повторяющегося кода и помещение этого кода в классы с целью последующего использования при создании объектов, обладающим определенным набором свойств и метедов. Это делается с целью упрощения работы над множеством однотипных объектов, а также с целью уменьшения вероятности допущения ошибок, когда изменяются общие для этих объектов свойства. 
2.1 Плюсы объектов 
    - при изменении методов и свойств у класса (добовлении методов и свойств) у созданных на основе этих классов объектов также меняются (добавляются, удаляются) определенные свойства и методы.
    - меньше кода на создание какой либо сущности
2.2 Минусы объектов
    - если появиласть необходимость переопределить какие либо методы в объекте, добавить их или удалить, то это нельзя сделать напрямую
    - при какой-либо ошибки в классе эта ошибка автоматически распространяется на все объекты, созданные на основе этого класса. -->
<?php

    class MachineClass 
    { 
        public $name;
        public $material;
        public $color;
        public $sound;

        public function getSoundOfWork() 
        {
            echo "Я работаю со звуком: " . $this->sound;    
        }
    }

    $washer = new MachineClass();
    $washer->name = "indesit";
    $washer->material = "plastic";
    $washer->color = "white";
    $washer->sound = "ж-ж-ж";
    echo "Я машина " . $washer->name . ". "; 
    echo $washer->getSoundOfWork() . ".<br>";


    $car = new MachineClass();
    $washer->name = "Запорожец";
    $washer->material = "metal";
    $washer->color = "black";
    $washer->sound = "бр-бр-бр";
    echo "Я машина " . $washer->name . ". "; 
    echo $washer->getSoundOfWork() . ".<br>"; 

    class TVClass 
    { 
        public $name;
        public $diagonalSize;
        public $screenResolution;
        public $energyConsumption;

        public function getPowerConsumption() 
        {
            echo "При работе 120 часов в месяц я израсходую " . $this->energyConsumption * 120/1000 . " кВт";    
        }
    }

    $Lg = new TVClass();
    $Lg->name = "Lg";
    $Lg->diagonalSize = 40;
    $Lg->screenResolution = "4k";
    $Lg->energyConsumption = 50;  
    echo $Lg->getPowerConsumption() . ".<br>";

    $Samsung = new TVClass();
    $Samsung->name = "Samsung";
    $Samsung->diagonalSize = 40;
    $Samsung->screenResolution = "5k";
    $Samsung->energyConsumption = 60;  
    echo $Samsung->getPowerConsumption() . ".<br>";

    class BallpenClass 
    { 
        public $name;
        public $color;      

        public function getLetter() 
        {
            echo "Цвет моих чернил " . $this->color;    
        }
    }

    $pilot = new BallpenClass();  
    $pilot->name = "pilot";
    $pilot->color = "синий";   
    echo $pilot->getLetter() . ".<br>";

    $bic = new BallpenClass();  
    $bic->name = "bic";
    $bic->color = "черный";   
    echo $bic->getLetter() . ".<br>";

    class DuckClass 
    { 
        public $weight;
        public $habitat;
        public $age;   

        public function getMeat() 
        {
            echo "Я утка весом " . $this->weight . " кг и из меня получится " . $this->weight * 0.5 . " кг мяса";    
        }
    }

    $donald = new DuckClass();  
    $donald->weight = 5;
    $donald->habitat = "farm";
    $donald->age = "2 года";  
    echo $donald->getMeat() . ".<br>";

    $scrooge = new DuckClass();  
    $scrooge->weight = 6;
    $scrooge->habitat = "wild nature";
    $scrooge->age = "3 года";  
    echo $scrooge->getMeat() . ".<br>";

    class ProductClass 
    { 
        public $name;
        private $price;    
        public $category;

        public function __construct($name, $price, $category)  
        {
           $this->name = $name;
           $this->price = $price;
           $this->category = $category;        
        } 

        public function pricing() 
        {
            if ($this->price > 10000) {
                echo "Ты что, Сдурел?.. такое дорогое покупать!";
            } else {
                echo "Покупай, нормально по цене";
            };    
        }
    }

    $coat = new ProductClass("coat", 1000, "clothes");
    echo $coat->pricing() . ".<br>";

    $car = new ProductClass("Mercedes", 50000, "cars");
    echo $car->pricing() . ".<br>";  

?>