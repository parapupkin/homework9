﻿<?php
    class NewsClass 
  { 
      public $caption;
      public $text;    
      public $category;
      public $author;
      public $date;

      public function __construct($caption, $text, $category, $author, $date)  
      {
          $this->caption = $caption;
          $this->text = $text;
          $this->category = $category;  
          $this->author = $author; 
          $this->date = $date;       
      } 

      public function getCaption()
      {
          return $this->caption;
      }

      public function getText()
      {
          return $this->text;
      }

      public function getCategory()
      {
          return $this->category;
      }

      public function getAuthor()
      {
          return $this->author;
      }

      public function getDate()
      {
          return $this->date;
      }   
  }


$content = file_get_contents(__DIR__ . "/news.json");
$news = json_decode($content, true);
 
?>
 
<!doctype html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <title>News</title>
</head>
<body>
<?php foreach ($news as $article){
  $newArticle = new NewsClass($article['caption'], $article['text'], $article['category'], $article['author'], $article['date']); ?>   
  <div>
    <h2><?= $newArticle->getCaption(); ?></h2>    
    <p><?= $newArticle->getText(); ?></p>
    <p>Вы читали новость из раздела <b><?= $newArticle->getCategory(); ?></b>, автор которой <b><?= $newArticle->getAuthor(); ?></b> дата публикации <b><?= $newArticle->getDate(); ?></b></p>
  </div>
<?php }?>
</body>
</html>